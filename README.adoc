= Testing Farm Documentation
// :idprefix:
// :idseparator: -
// URIs:
:uri-project: https://testing-farm.gitlab.io/docs/root
:uri-org: https://gitlab.com/testing-farm
:uri-group: {uri-org}/docs
:uri-repo: {uri-group}/site
:uri-playbook: {uri-repo}/blob/main/antora-playbook.yml
:uri-antora-docs: https://docs.antora.org/antora/latest
:uri-antora-install: {uri-antora-docs}/install/install-antora/
:uri-antora-playbook: {uri-antora-docs}/playbook/playbook-schema/
:uri-antora-run: {uri-antora-docs}/run-antora/

Please see {uri-project}[the documentation here].

== Antora

If you want to generate the documentation yourself, you'll need {uri-antora-install}[the Antora CLI] and the https://gitlab.com/testing-farm/docs/ui[Testing Farm UI Template].

== Playbook

An {uri-antora-playbook}[Antora playbook] is responsible for generating this site. It contains the instructions a user wants to relay to an Antora site generator pipeline. These instructions include the content Antora should collect and the UI it should apply to the generated site.

The file [.path]_antora-playbook.yml_ in this repository is the playbook used to produce this site.

== Generate this site

Once you've installed Antora and {uri-playbook}[saved the playbook] ([.path]_antora-playbook.yml_) to a local directory, you're ready to {uri-antora-run}[run Antora].

To generate the site use:

[source,shell]
....
antora --fetch --cache-dir .cache/antora --attribute page-pagination= --redirect-facility gitlab --to-dir public antora-playbook.yml
....

Alternatively, you can run Antora via `npx` from the `npm` package without need to install it.

[source,shell]
....
npx antora --fetch --cache-dir .cache/antora --attribute page-pagination= --redirect-facility gitlab --to-dir public antora-playbook.yml
....

// TODO: Document how to use with https://gitlab.com/testing-farm/docs/ui[Testing Farm UI Template].

== Copyright and License

© 2022 Red Hat, Inc.

// What license do we use?
// Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
// See link:LICENSE[] to find the full license text.
