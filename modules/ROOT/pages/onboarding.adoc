= Onboarding
:keywords: Onboarding

Currently Testing Farm is open for:

* any Red Hat employee, team or project
* any Fedora or CentOS Stream contributor, team or SIG
* any public project, service or initiative which Red Hat or Fedora is maintaining or co-maintaining

To be able to use Testing Farm you need an API key. The authentication is expected to change with API version updates, follow the steps according to API version you are using.

[NOTE]
====
The `redhat` ranch is available only to Red Hat employees, Red Hat teams, or projects that Red Hat is maintaining. Other projects may use the `public` ranch.
====

[NOTE]
====
An API key always corresponds to a single ranch only. It is not possible to use it for multiple ranches. If you need access to both ranches, you will need two keys.
====

== API version `v0.1`

With the API version `v0.1` the onboarding is manual. Send an email mailto:tft@redhat.com[tft@redhat.com] with the following content:

[source, shell]
....
Subject: Testing Farm Onboarding Request - Team Name or Initiative Name or Project Name or Service Name
To: tft@redhat.com

Hi,

I would like to onboard to Testing Farm API version v0.1.

Contact person: Name Surname <email@example.com>
Ranch: public or redhat
IM: IRC channel or Google Chat room or Slack CoreOS channel

Description of the usage:
A brief summary what you plan to use Testing Farm for. How many test requests
you plan to ask for in a period of time.  If you have any special requests, feel
free to mention them.

Best regards,
You
....
